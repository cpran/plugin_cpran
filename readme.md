cpran
=====

Description
-----------

[CPrAN][] is the plugin manager for Praat. However, this plugin only serves as
an interface between the CPrAN client (which is used to connect to the server
and manage the local plugins) and Praat itself.

You should not install this plugin manually, since in itself it offers no
functionality. Instead, follow the instructions no the CPrAN website to install
the client, and let the client install this plugin with its `init` command.

Requirements
------------

* None

[cpran]: http://cpran.net
