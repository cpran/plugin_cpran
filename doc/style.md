# Style guide

The purpose of this document is to attempt to maintain a unified style for
code written and maintained by a number of different people. This will
hopefully minimize the time spent on editing minor changes back and forth.

Once this document reaches a stable state, please discuss changes _before_
making them by opening an issue in the issue tracker.

## General

* Use 2 spaces for indent (`  `) instead of tabs

  Tabs are displayed with different sizes in different editors, and using
  them for indentation will make it hard to vertically align similar elements.

* Vertically align similar elements

  ~~~ praat
  # Not this
  pitch = Get mean: 0, 0, "Hertz"
  time = Get time of maximum: 0, 0, "Hertz", "Parabolic"
  duration = Get total duration
  select table
  Set numeric value: row, "num", variable
  Set string value: row, "string", variable

  # But this
  pitch    = Get mean:            0, 0, "Hertz"
  time     = Get time of maximum: 0, 0, "Hertz", "Parabolic"
  duration = Get total duration
  select table
  Set numeric value: row, "num",    variable
  Set string value:  row, "string", variable
  ~~~

* Avoid redundant `from 1` in `for` loop declarations

  ~~~ praat
  # Not this
  n = numberOfSelected()
  for i from 1 to n
  endfor

  # But this
  n = numberOfSelected()
  for i to n
  endfor

  # Do use "from" when it makes sense
  n = numberOfSelected()
  for j from 0 to n-1
    i = n - j
  endfor
  ~~~

* No space between function names and arguments

  ~~~ praat
  # Bad
  position = index (string$, "a")

  # Good
  position = index(string$, "a")

  # If needed for clarity, prefer spaces after the parentheses
  position = index( string$, "a" )
  ~~~

* No space between variable names and indeces

  ~~~ praat
  # Bad
  array [1]
  array$ [1]

  # Good
  array[1]
  array$[1]
  ~~~

* Put spaces around operators, except the negation unary operator and its
  operand (unless the space improves readability)

  ~~~ praat
  # Bad
  a = if n>0 then n else n+(4*x) fi
  a = if ! numberOfSelected() then a else b fi

  # Good
  a = if n > 0 then n else n + (4 * x) fi
  a = if !numberOfSelected() then a else b fi
  ~~~

* Break long lines; try to keep them under 80 characters unless this results
  in code that is more difficult to understand.

* When breaking lines, continue with one level of indentation until the end
  of the "line":

  ~~~ praat
  table = Create Table with column names: "wide_table", 0,
    ... "speaker age gender location interests hobbies dislikes"
  ~~~

* Break long lines after operators (except for `and` and `or`)

* If the line that gets broken would normally start a new indentation level,
  put a blank line between it and the beginning of its block:

  ~~~ praat
  if variableExists("pitch")
    ... and (pitch != undefined)
    ... and (f0 != undefined)
    ... and (f0 > 0) and (f0 < 600)

    # Do something
  endif
  ~~~

* Prefer `and` and `or` to `&&` and `||`

* Break code blocks into "paragraphs" with blank lines between different
  chunks

* Use inline `if`s if the result of the condition is a single assignment

* Use `if` blocks if the conditional block is more complex

* Prefer `+=`, `-=`, `*=` and `/=` to their more verbose alternatives

* Use `==` when comparing values and `=` when assigning values. These are
  different operations and should have different operators.

  ~~~ praat
  a = if var = undefined then 0 else var fi  ; Bad
  a = if var == undefined then 0 else var fi ; Good!
  ~~~

* Use logical conditions for your conditional blocks. This means choosing
  reasonable and appropriately named boolean variables, and setting up the
  conditions so that they are easily understood, not easily typed.

  ~~~ praat
  # Not this
  if false
  else
    # It is true!
  endif

  # But this
  if !false
    # True
  endif

  # And even better
  if true
    # True
  endif
  ~~~

* Make use of automatic boolean conversions for improved readability

  ~~~ praat
  # Bad
  n = numberOfSelected()
  if n == 0
    appendInfoLine: "No Sound objects"
  endif

  # Good
  sounds = numberOfSelected()
  if !sounds
    appendInfoLine: "No Sound objects"
  endif
  ~~~

* Omit redundant operation punctuation (eg. parentheses) as long as clarity
  doesn't suffer

* Keep variable names short and sweet: one or two words is ideal, three if you
  must. More probably means that there is a problem.

* Use a consistent naming scheme throughout

  * Use `underscore_spacing` instead of `camelCasing` for variable names

  * Use `camelCasing` instead of `underscore_spacing` for procedure names

  * But more important than this, stick to whatever you decide, and make it
    predictable!

* Try to keep the selection unchanged: if you change it, change it back, unless
  there is a good reason to not do so

* Try to mimic Praat's standard behaviour when possible:

  * New objects are selected (eg. after a procedure call)

  * Commands that take arguments end with an ellipsis ("...")

  * If a procedure takes object IDs as arguments, prefer using the active
    selection to pass the IDs (unless there are reasons for the opposite).

    ~~~ praat
    # Not this
    sound    = selected("Sound")
    textgrid = selected("TextGrid")
    # [...]
    procedure myproc: .sound, .textgrid
      # [...]
    endproc

    # But this
    selectObject: sound, textgrid
    procedure myproc ()
      .sound    = selected("Sound")
      .textgrid = selected("TextGrid")
      # [...]
    endproc
    ~~~

* Keep paths platform-agnostic:

  * Use `/` as a separator regardless of your platform

  * Do not hard-code paths into scripts (unless necessary)

  * If you _must_ use paths, use **relative** paths

* Be [DRY][]: Don't Repeat Yourself

  * Do not be afraid of breaking a long, complex task into smaller, simpler
    pieces of code. Use procedures and separate scripts to do this.

  * If a similar task is being called in two separate places, maybe that task
    represents some more abstract action that can be made independent. Try to
    think about the things those two have in common.

[dry]: https://en.wikipedia.org/wiki/Don%27t_repeat_yourself
