# Setup script for cpran
#
# Find the latest version of this plugin at
# https://gitlab.com/cpran/cpran-base
#
# Written by Jose Joaquín Atria
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# A copy of the GNU General Public License is available at
# <http://www.gnu.org/licenses/>.

Add menu command... "Objects" "Praat" "CPrAN" "" 0 
Add menu command... "Objects" "Help"  "CPrAN" "" 0 
